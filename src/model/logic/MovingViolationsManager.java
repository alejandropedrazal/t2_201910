
package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;
import model.data_structures.MovingViolations;

public class MovingViolationsManager implements IMovingViolationsManager {

	private LinkedList<VOMovingViolations> lista;
	
	public void loadMovingViolations(String movingViolationsFile){
		try 
		{
			lista=new LinkedList<VOMovingViolations>();
			CSVReader cr=new CSVReader(new FileReader(new File(movingViolationsFile)));
			String[] s=cr.readNext();
			s=cr.readNext();
			while(s!=null)
			{
				VOMovingViolations v=new VOMovingViolations(Integer.parseInt(s[0]), s[2], s[13], Integer.parseInt(s[9]), s[12], s[15], s[14]);
				lista.add(v);
				s=cr.readNext();
			}		
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

		
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> list=new LinkedList<>();
		Iterator<VOMovingViolations> it=lista.iterator();
		while(it.hasNext())
		{
			VOMovingViolations mv=it.next();
			if(mv.getViolationCode().equals(violationCode))
				list.add(mv);
		}
		
		return list;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator)
	{
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> list=new LinkedList<>();
		Iterator<VOMovingViolations> it=lista.iterator();
		while(it.hasNext())
		{
			VOMovingViolations mv=it.next();
			if(mv.getAccidentIndicator().equals(accidentIndicator))
				list.add(mv);
		}
		return list;
	}	


}
