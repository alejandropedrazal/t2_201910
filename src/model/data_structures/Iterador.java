package model.data_structures;

import java.util.Iterator;

public class Iterador<E> implements Iterator<E>
{
	/**
	 * El nodo donde se encuentra el iterador.
	 */
	private Node<E> actual;

	
	public Iterador(Node<E> primerNodo) 
	{
		actual = primerNodo;
	}
	
	/**
     * Indica si a�n hay elementos por recorrer
     * @return true en caso de que  a�n haya elemetos o false en caso contrario
     */
	public boolean hasNext() 
	{
		return actual != null;
	}

	/**
     * Devuelve el siguiente elemento a recorrer
     * <b>post:</b> se actualizado actual al siguiente del actual
     * @return objeto en actual
     */
	public E next()
	{
		if(actual != null)
		{
			E valor = actual.darElemento();
			actual = actual.darSiguiente();
			return valor;
		}
		return null;
	}
}
