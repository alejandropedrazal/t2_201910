package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class LinkedList<E> implements Iterable<E>
{
	private Node<E> primero;
	private int size;
	private Node<E> ultimo;
	
	public LinkedList() 
	{
		clear();
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardará al elemento que llega por parámetro. Actualiza el número de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public LinkedList(E nPrimero)
	{
		primero=new Node<E>(nPrimero);
		if(primero!=null)
			size++;
	}
	
	public boolean add(E elemento)
	{
		if(elemento==null)
			return false;
		else
		{
			Node<E> nodo=new Node<E>(elemento);
			if(primero!=null)
				nodo.cambiarSiguiente(primero);
			else
				ultimo=nodo;
			primero=nodo;	
			size++;
			return true;
		}	
	}

	/**
	 * Agrega un elemento al final de la lista, actualiza el número de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean addAtEnd(E elemento) throws NullPointerException
	{
		if(elemento==null)
		{
			throw new NullPointerException("El elemento es nulo");
		}
		Node<E> nuevo=new Node<E>(elemento);
		if(primero==null)
		{
			primero=nuevo;
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);
			ultimo=nuevo;
		}
		ultimo=nuevo;
		size++;
		return true;
	}

	/**
	 * Agrega un elemento en la posición dada de la lista. Todos los elementos siguientes se desplazan.
	 * Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param pos la posición donde se desea agregar. Si pos es igual al tamañoo de la lista se agrega al final
	 * @param elem el elemento que se desea agregar
	 * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
	 * @throws NullPointerException Si el elemento que se quiere agregar es null.
	 */
	public void addAtK(int index, E elemento) throws IndexOutOfBoundsException, NullPointerException
	{
		boolean agregado=false;
		if(index<0 || index>size())
		{
			throw new IndexOutOfBoundsException("La posición es incorrecta");
		}
		if(elemento==null)
		{
			throw new NullPointerException("El elemento es nulo");
		}
		Node<E> elem=new Node<E>(elemento);
		if(primero!=null)
		{		
			if(index==0)
			{				
				elem.cambiarSiguiente(primero);
				primero=elem;
				agregado=true;
				size++;
			}
			else if(index==size)
			{
				ultimo.cambiarSiguiente(elem);
				ultimo=elem;
				size++;
			}
			else
			{
				int i=0;
				Node<E> actual=primero;
				Node<E> anterior=null;
				while(!agregado && actual!=null && actual.darSiguiente()!=null)
				{
					if(i==index && !contains(elem))
					{
						elem.cambiarSiguiente(actual);
						anterior.cambiarSiguiente(elem);
						agregado=true;
						size++;
					}
					else
					{
						anterior=actual;
						actual=actual.darSiguiente();
						i++;
					}
				}
			}
		}
		else if(index==0)
		{
			primero=elem;
			size++;
		}
	}

	/**
	 * Borra todos los elementos de la lista. Actualiza la cantidad de elementos en 0
	 * <b>post:</b> No hay elementos en la lista
	 */
	public void clear() 
	{
		size=0;
		primero=null;
	}

	/**
	 * Indica si la lista contiene el objeto indicado
	 * @param objeto el objeto que se desea buscar en la lista. objeto != null
	 * @return true en caso que el objeto está en la lista o false en caso contrario
	 */
	public boolean contains(Object o) 
	{
		boolean hay=false;
		Node<E> actual=primero;
		while(actual!=null && !hay)
		{
			if(actual.darElemento().equals(o))
			{
				hay=true;
			}
			actual=actual.darSiguiente();
		}
		return hay;
	}

	/**
	 * Indica si la lista contiene todos los objetos de la colección dada
	 * @param coleccion la colección de objetos que se desea buscar. coleccion !=null
	 * @return true en caso que todos los objetos están en la lista o false en caso contrario
	 */
	public boolean containsAll(Collection<?> c) 
	{
		boolean contiene = true;
		for(Object objeto: c)
		{
			contiene &= contains(objeto);
		}
		return contiene;
	}

	/**
	 * Devuelve el nodo de la posición dada
	 * @param pos la posición  buscada
	 * @return el nodo en la posición dada 
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public Node<E> darNodo(int index)
	{
		if(index < 0 || index > size)
		{
			throw new IndexOutOfBoundsException("Se está pidiendo el indice: " + index + " y el tamaño de la lista es de " + size);
		}
	
		Node<E> actual = primero;
		int posActual = 0;
		while(actual != null && posActual < index)
		{
			actual = actual.darSiguiente();
			posActual ++;
		}
	
		return actual;
	}

	/**
	 * Devuelve el elemento de la posición dada
	 * @param pos la posición  buscada
	 * @return el elemento en la posición dada 
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E getElement(int index) throws IndexOutOfBoundsException
	{
		if(index<0 || index>=size())
		{
			throw new IndexOutOfBoundsException("La posición es incorrecta");
		}
		if(index==0)
			return primero!=null?primero.darElemento():null;
		else if(index==size-1)
			return ultimo!=null?ultimo.darElemento():null;
		E elemento=null;
		if(primero!=null)
		{
			Node<E> actual=primero.darSiguiente();
			while(actual!=null && actual.darSiguiente()!=null && elemento==null)
			{
				if(indexOf(actual)==index)
				{
					elemento=actual.darElemento();
				}
				actual=actual.darSiguiente();
			}
		}
		return elemento;
	}

	public Integer getSize()
	{
		return size;
	}

	/**
	 * Indica la posición en la lsita del objeto que llega por parámetro
	 * @param objeto el objeto que se desea buscar en la lista. objeto != null
	 * @return la posición del objeto o -1 en caso que no se encuentre en la lista
	 */
	public int indexOf(Object o) 
	{
		int posicion=-1;
		int cont=0;
		if(primero!=null)
		{
			Node<E> actual=primero;
			while(actual!=null && posicion==-1)
			{
				if(actual.equals(o))
				{
					posicion=cont;
				}
				else if(actual.darElemento().equals(o))
				{
					posicion=cont;
				}
				cont++;
				actual=actual.darSiguiente();
			}
		}
		return posicion;
	}

	public Iterator<E> iterator() 
	{
		return new Iterador<E>(primero);
	}

	/**
	 * Indica si la lista está vacia
	 * @return true si la lista está vacia o false en caso contrario
	 */
	public boolean isEmpty() 
	{
		return primero==null?true:false;
	}

	/**
	 * Indica la  última posición  donde aparece el objeto por parámetro en la lista
	 * @param objeto el objeto buscado en la lista. objeto != null
	 * @return la  última posición del objeto en la lista o -1 en caso que no exista
	 */
	public int lastIndexOf(Object o)
	{
		int ultimaPosicion = -1;
		Node<E> actual = primero;
		int posActual = 0;
		while(actual != null)
		{
			if(actual.darElemento().equals((o)))
			{
				ultimaPosicion = posActual;
			}
			posActual ++;
			actual = actual.darSiguiente();
		}
	
		return ultimaPosicion;
	}

	@Deprecated
	public ListIterator<E> listIterator() 
	{
		throw new UnsupportedOperationException ();
	}

	@Deprecated
	public ListIterator<E> listIterator(int index) 
	{
		throw new UnsupportedOperationException ();
	}


	/**
	 * Elimina el nodo en la posición por parámetro. Actualiza la cantidad de elementos.
	 * @param pos la posición que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
	 */
	public E removeAtK(int pos) throws IndexOutOfBoundsException
	{
		if(pos<0 || pos>=size())
		{
			throw new IndexOutOfBoundsException("La posición es incorrecta");
		}
		Node<E> eliminado=null;
		if(primero!=null)
		{
			if(pos==0)
			{
				eliminado=primero;
				primero=primero.darSiguiente();
				size--;
			}
			else
			{
				Node<E> actual=primero;
				int i=1;
				while(actual!=null && actual.darSiguiente()!=null && eliminado==null)
				{
					if(i==pos)
					{
						eliminado=actual.darSiguiente();
						actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
						if(actual.darSiguiente()==null)
							ultimo=actual;
						size--;
					}
					actual=actual.darSiguiente();
					i++;
				}
			}
		}
		return eliminado.darElemento();
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por parámetro. Actualiza el número de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	public boolean remove(Object objeto) 
	{
		boolean elimino=false;
		if(primero!=null)
		{
			if(primero.darElemento().equals(objeto))
			{
				primero=primero.darSiguiente();
				elimino=true;
				size--;
			}
			else
			{
				Node<E> actual=primero;
				while(actual!=null && actual.darSiguiente()!=null && !elimino)
				{
					if(actual.darSiguiente().darElemento().equals(objeto))
					{
						actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
						if(actual.darSiguiente()==null)
							ultimo=actual;
						elimino=true;
						size--;
					}
					actual=actual.darSiguiente();
				}
			}
		}
		return elimino;
	}
	
	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro. La cantidad de elementos se actualiza.
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> coleccion) 
	{
		boolean elimino=false;
		if(primero!=null)
		{
			int c=size;
			elimino=true;
			boolean esta=false;
			while(primero!=null && !esta)
			{
				if(coleccion.contains(primero.darElemento()))
				{
					esta=true;
				}
				else
				{
					primero=primero.darSiguiente();
					size--;
				}
			}
			Node<E> actual=primero;
			while(actual!=null && actual.darSiguiente()!=null)
			{
				if(!coleccion.contains(actual.darSiguiente().darElemento()))
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					size--;
				}
				else
				{
					actual=actual.darSiguiente();
				}
			}
			actual=primero;
			while(actual!=null)
			{
				if(!coleccion.contains(actual.darElemento()))
				{
					elimino=false;
				}
				actual=actual.darSiguiente();
			}
			if(c==size)
			{
				elimino=false;
			}
		}
		return elimino;
	}

	/**
	 * Reemplaza el elemento de la posición por el elemento que llega por parámetro.
	 * @param index La posición en la que se desea reemplazar el elemento.
	 * @param element El nuevo elemento que se quiere poner en esa posición
	 * @return el método que se ha retirado de esa posición.
	 * @throws IndexOutOfBoundsException si la posición es < 0 o la posición es >= size()
	 */
	public E set(int index, E element) throws IndexOutOfBoundsException 
	{
		if(index<0 || index>=size())
		{
			throw new IndexOutOfBoundsException("La posición es incorrecta");
		}
		E retirado=null;
		Node<E> anterior=null;
		Node<E> ele=new Node<E>(element);
		if(primero!=null)
		{
			Node<E> actual=primero;
			int i=0;
			while(actual!=null && retirado==null)
			{
				if(i==index)
				{
					if(anterior!=null)
					{
						retirado=actual.darElemento();
						ele.cambiarSiguiente(actual.darSiguiente());
						anterior.cambiarSiguiente(ele);
					}
					else
					{
						retirado=actual.darElemento();
						ele.cambiarSiguiente(actual.darSiguiente());
						primero=ele;
					}
				}
				anterior=actual;
				actual=actual.darSiguiente();
				i++;
			}
		}
		return retirado;		
	}

	/**
	 * Indica el tamaño de la lista.
	 * @return La cantidad de nodos de la lista.
	 */
	public int size() 
	{
		return size;
	}	
}
