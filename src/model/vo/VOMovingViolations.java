package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations 
{
	

	private int objectId;
	private String location;
	private String tickectIssue;
	private int totalPaid;
	private String accidentIndicator;
	private String violationDescription;
	private String violationCode;
	
	public VOMovingViolations(int objectId, String location, String tickectIssue, int totalPaid, String accidentInficator, String violationDescription, String violationCode)
	{
		this.objectId=objectId;
		this.location=location;
		this.tickectIssue=tickectIssue;
		this.totalPaid=totalPaid;
		this.accidentIndicator=accidentInficator;
		this.violationDescription=violationDescription;
		this.violationCode=violationCode;
	}
	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		return objectId;
	}	
	
	public void changeId(int id)
	{
		objectId=id;
	}
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() 
	{
		return location;
	}

	public void changeLocation(String location)
	{
		this.location=location;
	}
	
	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() 
	{
		return tickectIssue;
	}
	
	public void changeTickectIssue(String tickectIssue)
	{
		this.tickectIssue=tickectIssue;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() 
	{
		return totalPaid;
	}
	
	public void changeTotalPaid(int totalPaid)
	{
		this.totalPaid=totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		return accidentIndicator;
	}
		
	public void changeAccidentIndicator(String accidentIndicator)
	{
		this.accidentIndicator=accidentIndicator;
	}
	
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() 
	{
		return violationDescription;
	}
	
	public void changeViolationDescription(String violationDescription)
	{
		this.violationDescription=violationDescription;
	}
	
	
	public String getViolationCode()
	{
		return violationCode;
	}
	
	public void changeViolationCode(String violationCode)
	{
		this.violationCode=violationCode;
	}
}


